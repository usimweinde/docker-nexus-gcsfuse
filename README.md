## Build the container
```bash
docker build  -t nexus-gcsfuse .
```

## Run Docker container
To run this Docker image you need to setup
- a local directory for the nexus volume, e.g. /var/sonatype-work
- provide a symbolic link to the GCS bucket mount
  ```
ln -s /mnt/gcs /var/sonatype-work/storage 
```
  This is a little bit confusing: The symbolic link is created on the host, but
  used in the docker container.
- a local directory containing the JSON key file for use with GCS

Additionally, you need to provide the name of the GCS bucket as environment variables `GCS_BUCKET`.

The `--privileged` option is needed for gcsfuse. I have found no solution without 
it. Using `--device /dev/fuse --cap-add sys_admin` is not enough.

### Example:
```bash
docker run --name nexus -d -p 8081:8081 -v /var/sonatype-work:/sonatype-work:Z \
  -e "GCS_BUCKET=my-bucket"  \
  -v /some/local/dir/my-gcs-keyfile.json:/gcs-secret/keyfile.json:Z \
  --privileged nexus-gcsfuse
```
