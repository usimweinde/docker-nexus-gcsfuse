FROM sonatype/nexus:oss

# install gcsfuse and configure it
VOLUME ["/gcs-secret"]
COPY gcsfuse.repo /etc/yum.repos.d/gcsfuse.repo
USER root
RUN yum -y install gcsfuse
RUN echo user_allow_other > /etc/fuse.conf
RUN mkdir /mnt/gcs && chmod 777 /mnt/gcs
USER nexus

# run gcsfuse before starting nexus
CMD gcsfuse --key-file /gcs-secret/keyfile.json $GCS_BUCKET /mnt/gcs && \
  ${JAVA_HOME}/bin/java \
  -Dnexus-work=${SONATYPE_WORK} -Dnexus-webapp-context-path=${CONTEXT_PATH} \
  -Xms${MIN_HEAP} -Xmx${MAX_HEAP} \
  -cp 'conf/:lib/*' \
  ${JAVA_OPTS} \
  org.sonatype.nexus.bootstrap.Launcher ${LAUNCHER_CONF}
